<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    protected $table = 'setting';
    public $timestamps = false;
    public $primaryKey = 'id';
    protected $fillable = [
        'project_name', 'project_email', 'project_logo'
    ];
}
