<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    use HasFactory;
    protected $table = 'users';
    public $timestamps = false;
    public $primaryKey = 'id';
    protected $fillable = [
        'company_id', 'first_name', 'last_name', 'phone', 'email', 'password', 'status', 'role_id'
    ];
}
