<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class TokenCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('token');
        if (empty($token)) {
            return response()->json(['status' => false, 'error' => 'Token not Found'], 404);
        } else {
            if ($token != '1024db440a5b1b8fc4caa9cc416312d5102b8fc4caa9cc4164db440a5b1b8fc4caa9cc416312d5') {
                return response()->json(['status' => false, 'error' => 'Invalid Token'], 404);
            } else {
                return $next($request);
            }
        }
    }
}
