<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Image;

class CompanyController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'logo' => 'mimes:jpeg,jpg,png,gif|required|max:1024',
            'website' => 'required|url',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        } else {
            $company_check = Company::where('name', $request->name)->first();
            if (!empty($company_check)) {
                return response()->json(['status' => 404, 'msg' => 'Company name already exist']);
            } else {
                $image = $request->file('logo');
                if ($image) {
                    $file = $request->file('logo');
                    $extention = $file->getClientOriginalExtension();
                    $filename = time() . rand(0, 999) . '.' . $extention;
                    $img = Image::make($image->path());
                    $img->resize(100, 100);
                    $file->move('uploads/images/logo/', $filename);
                    $db = '/uploads/images/logo/' . $filename;
                } else {
                    $db = null;
                }


                $data = Company::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'logo' => $db,
                    'website' => $request->website,
                ]);
                return response()->json(['status' => 200, 'msg' => 'Company Added Successfully', 'company_id' => $data->id]);
            }
        }
    }
    public function get_company(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        } else {
            $data = Company::where('id', $request->company_id)->first();
            if (!empty($data)) {
                return response()->json(['status' => 200, 'msg' => 'Data Found', 'data' => $data]);
            } else {
                return response()->json(['status' => 404, 'msg' => 'No Data Found']);
            }
        }
    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'logo' => 'mimes:jpeg,jpg,png,gif|required|max:1024',
            'website' => 'required|url',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        } else {

            $company = Company::where('id', $request->id)->first();
            if (empty($company)) {
                return response()->json(['status' => 404, 'msg' => 'No Record Found']);
            } else {
                $image = $request->file('logo');
                if ($image) {
                    $file = $request->file('logo');
                    $extention = $file->getClientOriginalExtension();
                    $filename = time() . rand(0, 999) . '.' . $extention;
                    $img = Image::make($image->path());
                    $img->resize(100, 100);
                    $file->move('uploads/images/logo/', $filename);
                    $db = '/uploads/images/logo/' . $filename;
                    Company::where('id', $request->id)
                        ->update([
                            'logo' => $db,
                        ]);
                }
                Company::where('id', $request->id)
                    ->update([
                        'name' => $request->name,
                        'email' => $request->email,
                        'website' => $request->website,
                    ]);
                $data = Company::where('id', $request->id)->first();
                return response()->json(['status' => 200, 'msg' => 'Company Updated Successfully', 'data' => $data]);
            }
        }
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        } else {
            $company = Company::where('id', $request->id)->first();
            if (empty($company)) {
                return response()->json(['status' => 404, 'msg' => 'No Record Found']);
            } else {
                Company::where('id', $request->id)
                    ->delete();
                return response()->json(['status' => 200, 'msg' => 'Company Deleted Successfully']);
            }
        }
    }
}
