<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|numeric',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|numeric|digits:10',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        } else {
            $company_check = Users::where('first_name', $request->first_name)->first();
            if (!empty($company_check)) {
                return response()->json(['status' => 404, 'msg' => 'Company name already exist']);
            } else {
                $role = Role::where('role', 'Employee')->first();
                $role_id = $role->id;
                $data = Users::create([
                    'company_id' => $request->company_id,
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'phone' => $request->phone,
                    'email' => $request->email,
                    'role_id' => $role_id,
                ]);
                return response()->json(['status' => 200, 'msg' => 'Employee Added Successfully', 'employee_id' => $data->id]);
            }
        }
    }
    public function get_employee(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        } else {
            $data = Users::leftjoin('role', 'role.id', 'users.role_id')
                ->leftjoin('company', 'company.id', 'users.company_id')
                ->where('role', '!=', 'Admin')
                ->where('users.id', $request->employee_id)
                ->select('users.id', 'users.first_name', 'users.last_name', 'users.email', 'users.phone')
                ->first();
            if (!empty($data)) {
                return response()->json(['status' => 200, 'msg' => 'Data Found', 'data' => $data]);
            } else {
                return response()->json(['status' => 404, 'msg' => 'No Data Found']);
            }
        }
    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee_id' => 'required|numeric',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|numeric|digits:10',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        } else {
            $company = Users::leftjoin('role', 'role.id', 'users.role_id')
                ->where('role', '=', 'Employee')
                ->where('users.id', $request->employee_id)
                ->first();
            if (empty($company)) {
                return response()->json(['status' => 404, 'msg' => 'No Record Found']);
            } else {
                Users::where('id', $request->employee_id)
                    ->update([
                        'first_name' => $request->first_name,
                        'last_name' => $request->last_name,
                        'phone' => $request->phone,
                        'email' => $request->email,
                    ]);
                $data = Users::leftjoin('role', 'role.id', 'users.role_id')
                    ->where('role', '=', 'Employee')
                    ->where('users.id', $request->employee_id)
                    ->select('users.first_name', 'users.last_name', 'users.email', 'users.phone')
                    ->first();
                return response()->json(['status' => 200, 'msg' => 'Company Updated Successfully', 'data' => $data]);
            }
        }
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        } else {
            $company = Users::leftjoin('role', 'role.id', 'users.role_id')
                ->where('role', '=', 'Employee')
                ->where('users.id', $request->id)->first();
            if (empty($company)) {
                return response()->json(['status' => 404, 'msg' => 'No Record Found']);
            } else {
                Users::leftjoin('role', 'role.id', 'users.role_id')
                    ->where('role', '=', 'Employee')
                    ->where('users.id', $request->id)
                    ->delete();
                return response()->json(['status' => 200, 'msg' => 'Employee Deleted Successfully']);
            }
        }
    }
}
