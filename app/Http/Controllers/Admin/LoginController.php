<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        return view('admin.login');
    }
    public function dashboard(Request $request)
    {
        return view('admin.dashboard');
    }
    public function logout(Request $request)
    {
        Session::flush();
        return redirect('/control/login');
    }
    public function login_check(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $data = Users::join('role', 'role.id', 'users.role_id')
            ->where('users.email', $email)
            ->where('users.password', md5($password))
            ->where('role', 'Admin')
            ->first();
        if (!empty($data)) {
            Session::put('backend', [
                'id' => $data->id,
                'email' => $data->email,
                'role' => $data->role,
            ]);
            return redirect('/control')->with('success', 'Welcome');
        } else {
            return redirect()->back()->with('error', 'Invalid credentials');
        }
    }
    public function setting(Request $request)
    {
        $setting = Setting::where('id', 1)->first();
        return view('admin.setting', compact('setting'));
    }
    public function setting_save(Request $request)
    {
        $request->validate([
            'project_name' => 'required|max:255',
            'project_email' => 'required|email|max:255'
        ]);
        if ($request->project_logo) {
            $file = $request->file('project_logo');
            $extention = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extention;
            $file->move('uploads/setting/', $filename);
            $db = '/uploads/setting/' . $filename;
            Setting::where('id', 1)
                ->update([
                    'project_logo' => $db,
                ]);
        }
        Setting::where('id', 1)
            ->update([
                'project_name' => $request->project_name,
                'project_email' => $request->project_email,
            ]);

        return redirect()->back()->with('success', 'Details updated successfully');
    }
    public function change_password(Request $request)
    {
        $id = Session::get('backend')['id'];
        $data = Users::where('id', $id)->first();
        return view('admin.change-password', compact('data'));
    }
    public function password_save(Request $request)
    {
        $id = $request->id;
        $old_password = $request->old_password;
        $new_password = md5($request->new_password);
        $old_check = Users::where('id', $id)
            ->where('password', md5($old_password))
            ->first();
        if (!empty($old_check)) {
            Users::where('id', $id)
                ->update([
                    'password' => $new_password
                ]);
            return redirect()->back()->with('success', 'Password Change Successfully');
        } else {
            return redirect()->back()->with('error', 'Old Password does not match');
        }
    }
}
