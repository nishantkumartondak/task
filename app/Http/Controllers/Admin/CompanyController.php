<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function user_list(Request $request)
    {
        $list = Company::paginate(10);
        return view('admin.company.list', compact('list'));
    }
}
