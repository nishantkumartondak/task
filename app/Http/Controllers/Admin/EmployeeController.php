<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;

class EmployeeController extends Controller
{
    public function employee_list(Request $request)
    {
        $list = Users::leftjoin('role', 'role.id', 'users.role_id')
            ->leftjoin('company', 'company.id', 'users.company_id')
            ->where('role.role', '=', 'Employee')
            ->select('users.*','company.name as company_name')
            ->paginate(10);
        return view('admin.employee.list', compact('list'));
    }
}
