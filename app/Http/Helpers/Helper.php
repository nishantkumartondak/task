<?php

namespace App\Http\Helpers;

class Helper
{
    public static function sum($a, $b)
    {
        return $a + $b;
    }
    public static function multi($a, $b)
    {
        return $a * $b;
    }
    public static function dateandtime()
    {
        date_default_timezone_set('Asia/Kolkata');
        return date('d-m-Y H:i:s');
    }
}
