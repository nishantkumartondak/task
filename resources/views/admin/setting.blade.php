@extends('admin.layout.app')
@section('title','Setting')
@section('content')
<div class="card-body">

    <form action="{{url('/control/setting-save')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="form-group col-lg-6">
                <label>Name</label>
                <input class="form-control" type="text" name="project_name" value="{{$setting->project_name}}">
            </div>
            <div class="form-group col-lg-6">
                <label>Email</label>
                <input class="form-control" type="text" name="project_email" value="{{$setting->project_email}}">
            </div>
            <div class="form-group col-lg-6">
                <label>Image</label><br>
                <img src="{{asset($setting->project_logo)}}" alt="" width="150" height="120">
            </div>
            <div class="form-group col-lg-6">
                <label>Change Image</label><br>
                <input type="file" name="project_logo">
            </div>
            <div class="form-group col-lg-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
    </form>
</div>
</div>
@endsection