@extends('admin.layout.app')
@section('title','Change Password')
@section('content')
<div class="card-body">
    <form action="{{url('/control/password-save')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="form-group col-lg-6">
                <label>Old Password</label>
                <input type="hidden" name="id" value="{{$data->id}}">
                <input class="form-control" type="password" name="old_password" placeholder="Old Password" required>
            </div>
            <div class="form-group col-lg-6">
                <label>New Password</label>
                <input class="form-control" type="password" name="new_password" placeholder="New Password" required>
            </div>
            <div class="form-group col-lg-12 text-center mt-5">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection