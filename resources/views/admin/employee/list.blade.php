@extends('admin.layout.app')
@section('title','Employee List')
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Employee List</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/control')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Employee List</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">

    <!-- Default box -->
    <div class="card p-4">
        <div class="card-header">
            <h3 class="card-title">Employee List</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body p-0">
            <table class="table table-striped projects">
                <thead>
                    <tr>
                        <th style="width: 1%" class="text-center">S.No</th>
                        <th style="width: 15%" class="text-center">Firstname</th>
                        <th style="width: 15%" class="text-center">Lastname</th>
                        <th style="width: 15%" class="text-center">Phone</th>
                        <th style="width: 20%" class="text-center">Email</th>
                        <th style="width: 20%" class="text-center">Company</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($list as $i)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td class="text-center">{{$i->first_name}}</td>
                        <td class="text-center">{{$i->last_name}}</td>
                        <td class="text-center">{{$i->phone}}</td>
                        <td class="text-center">{{$i->email}}</td>
                        <td class="text-center">{{$i->company_name}}</td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
            {{$list->links()}}
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

</section>
@endsection