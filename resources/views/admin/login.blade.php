<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <title>Login!</title>
  <!-- toastr -->
  <link rel="stylesheet" href="{{asset('/plugins/toastr/toastr.min.css')}}">
</head>

<body style="background-color: #7952b3;color:white;">
  <div class="container">
    <div class="row">

      <div class="col-lg-12" style="padding: 10% 10% 10% 10%;">
        @include('admin.layout.error-message')
        <h1 class="text-center">Login Here!</h1>
        <form action="{{url('/control/login-check')}}" method="post" style="padding-left:25%; padding-right:25%;padding-top:5%">
          @csrf
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Email</label>
            <input type="text" class="form-control" name="email" required>
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Password</label>
            <input type="password" class="form-control" name="password" required>
          </div>
          <div class="mt-5 text-center">
            <button type="submit" class="btn btn-lg btn-success text-white">Login</button>
            <div>
        </form>
      </div>
    </div>
  </div>
  <!-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script> -->

  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <!-- <script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script> -->
  <!-- <script src="{{asset('plugins/jqvmap/jquery.vmap.min.js')}}"></script> -->

  <!-- toastr -->
  <script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>


  <!-- <script>
    toastr.success('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
  </script> -->
  <script>
    function mydf() {
      var x = document.getElementById('alert-message');
      x.style.display = 'none';
    }
  </script>
  <script>
    $(function() {
      setTimeout(function() {
        $("#alert-message").hide('blind', {}, 500)
      }, 1000);
    });
    $(function() {
      setTimeout(function() {
        $(".alert-danger").hide('blind', {}, 500)
      }, 1000);
    });
  </script>

</body>

</html>