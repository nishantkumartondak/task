@if(Session::has('success'))
<div class="alert alert-success position-absolute justify-content-center" style="z-index: 1;right:0px; margin:auto;width: 25%; text-align:center" id="alert-message">{{Session::get('success')}}</div>

@endif
@if(Session::has('error'))
<div class="alert alert-danger position-absolute justify-content-center" style="z-index: 1;right:0px; margin:auto;width: 25%; text-align:center" id="alert-message">{{Session::get('error')}}</div>

@endif

@if ($errors->any())
@foreach ($errors->all() as $error)
<div class="alert alert-danger position-absolute justify-content-center" style="z-index: 1;right:0px; margin:auto;width: 25%; text-align:center">{{$error}}</div>

@endforeach
@endif