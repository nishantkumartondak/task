<?php

use App\Http\Controllers\API\CompanyController;
use App\Http\Controllers\API\EmployeeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});





Route::group(['middleware' => 'TokenCheck'], function () {

    //Company
    Route::post('company-register', [CompanyController::class, 'register']);
    Route::post('get-company', [CompanyController::class, 'get_company']);
    Route::post('company-update', [CompanyController::class, 'update']);
    Route::post('company-delete', [CompanyController::class, 'delete']);

    //Employee
    Route::post('employee-register', [EmployeeController::class, 'register']);
    Route::post('get-employee', [EmployeeController::class, 'get_employee']);
    Route::post('employee-update', [EmployeeController::class, 'update']);
    Route::post('employee-delete', [EmployeeController::class, 'delete']);
});



// api token:- '1024db440a5b1b8fc4caa9cc416312d5102b8fc4caa9cc4164db440a5b1b8fc4caa9cc416312d5'

// api collection link:- https://www.getpostman.com/collections/d77e8491ecd32c12fc6f