<?php

use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\EmployeeController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\TeamController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Front\FrontendController;
use App\Http\Controllers\Front\HomeController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/control', 'middleware' => 'PreventBackHistory'], function () {

    Route::get('login', [LoginController::class, 'login']);
    Route::post('login-check', [LoginController::class, 'login_check']);

    Route::group(['middleware' => 'CustomAuth'], function () {
        //Login Section
        Route::get('/', [LoginController::class, 'dashboard']);
        Route::get('logout', [LoginController::class, 'logout']);
        Route::get('setting', [LoginController::class, 'setting']);
        Route::post('setting-save', [LoginController::class, 'setting_save']);
        Route::get('change-password', [LoginController::class, 'change_password']);
        Route::post('password-save', [LoginController::class, 'password_save']);
        //Login Section
        Route::get('company-list', [CompanyController::class, 'user_list']);
        Route::get('employee-list', [EmployeeController::class, 'employee_list']);
    });
});
